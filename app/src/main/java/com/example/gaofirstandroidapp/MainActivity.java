package com.example.gaofirstandroidapp;
import androidx.appcompat.app.AppCompatActivity;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.lang.reflect.Field;
import java.util.ArrayList;

import java.io.*;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.example.gaofirstandroidapp.bean.DownloadFileTool;
import com.example.gaofirstandroidapp.FileDownloader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Retrofit;







public class MainActivity extends AppCompatActivity {

    int page = 1;
    int MaxPage = 14;
    int if_display = 0;
    int if_sleep = 1;
    private static final String TAG = "MainActivity";
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private HttpbinService httpbinService;
    private DownloadFileTool downloadFileTool;


    //
    public int getResourceByReflect(String imageName){
        Class drawable  =  R.drawable.class;
        Field field = null;
        int r_id ;
        try {
            field = drawable.getField(imageName);
            r_id = field.getInt(field.getName());
        } catch (Exception e) {
            r_id=R.drawable.sheetnull;
            Log.e("ERROR", "PICTURE NOT　FOUND！");
        }
        return r_id;
    }



    //读取json文件
    public String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            AssetManager manager = getAssets();
            //File jsonFile = new File(fileName);
            // FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(manager.open(fileName),"utf-8"); //new FileInputStream(jsonFile)
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            // fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        okHttpClient = new OkHttpClient();
        retrofit = new Retrofit.Builder().baseUrl("http://127.0.0.1/").build(); //https://www.httpbin.org/   127.0.0.1
        httpbinService = retrofit.create(HttpbinService.class);
        downloadFileTool = retrofit.create(DownloadFileTool.class);


        Button btn_previous = findViewById(R.id.button1);
        Button btn_next = findViewById(R.id.button2);
        Button btn_clear = findViewById(R.id.button3);
        Button btn_stop = findViewById(R.id.button14);
        Button btn_display = findViewById(R.id.button0);
        ImageView show_img = findViewById(R.id.iv1);

        ArrayList<Button> lightList = new ArrayList<Button>();
        Button button11 = findViewById(R.id.b11);
        lightList.add(button11);
        Button button12 = findViewById(R.id.b12);
        lightList.add(button12);
        Button button13 = findViewById(R.id.b13);
        lightList.add(button13);
        Button button14 = findViewById(R.id.b14);
        lightList.add(button14);
        Button button21 = findViewById(R.id.b21);
        lightList.add(button21);
        Button button22 = findViewById(R.id.b22);
        lightList.add(button22);
        Button button23 = findViewById(R.id.b23);
        lightList.add(button23);
        Button button24 = findViewById(R.id.b24);
        lightList.add(button24);
        Button button31 = findViewById(R.id.b31);
        lightList.add(button31);
        Button button32 = findViewById(R.id.b32);
        lightList.add(button32);
        Button button33 = findViewById(R.id.b33);
        lightList.add(button33);
        Button button34 = findViewById(R.id.b34);
        lightList.add(button34);
        Button button41 = findViewById(R.id.b41);
        lightList.add(button41);
        Button button42 = findViewById(R.id.b42);
        lightList.add(button42);
        Button button43 = findViewById(R.id.b43);
        lightList.add(button43);
        Button button44 = findViewById(R.id.b44);
        lightList.add(button44);
        Button button51 = findViewById(R.id.b51);
        lightList.add(button51);
        Button button52 = findViewById(R.id.b52);
        lightList.add(button52);
        Button button53 = findViewById(R.id.b53);
        lightList.add(button53);
        Button button54 = findViewById(R.id.b54);
        lightList.add(button54);
        Button button61 = findViewById(R.id.b61);
        lightList.add(button61);
        Button button62 = findViewById(R.id.b62);
        lightList.add(button62);
        Button button63 = findViewById(R.id.b63);
        lightList.add(button63);
        Button button64 = findViewById(R.id.b64);
        lightList.add(button64);





        for (int i = 0; i < lightList.size(); i++) {
            Button button_change = lightList.get(i);
            button_change.setBackgroundResource(R.drawable.button_border);
        }
/*        int[][] data={
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0},
                {0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,1,0,0,0,3,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,4,0,0,0,0,3,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };*/

        int[][] data={
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        int[] duration_data={
                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        };


        String path = "lightEventData.json"; // "C:"+File.separator+"lightEventData.json"
        String s = readJsonFile(path);
        JSONObject json_ob = JSON.parseObject(s);
        JSONArray press_data = json_ob.getJSONArray("light_event_data");


        // load the data to board array
        for(int i = 0; i < press_data.size(); i++){
            JSONObject key = (JSONObject) press_data.get(i);
            int key_duration = (int) key.get("durationTime");
            duration_data[i] = key_duration;
            JSONArray key_finger = (JSONArray) key.get("finger_data");
            for(int j = 0; j < key_finger.size(); j++){
                JSONArray key_array = (JSONArray) key_finger.get(j);
                List<Integer> key_java_list = JSON.parseArray(key_array.toString(),Integer.class);
                for(int k = 0; k < key_java_list.size(); k++){
                    data[i+2][k+4*j] = key_java_list.get(k);
                }
            }
        }
        System.out.println(data[0][17]);


/*        int num = 1;
        JSONObject key = (JSONObject) press_data.get(num);
        JSONArray key_finger = (JSONArray) key.get("finger_data");
        int key_duration = (int) key.get("durationTime");
        //JSON.parseArray(key_finger);
        JSONArray key_1 = (JSONArray) key_finger.get(1);
        List<Integer> int_list = JSON.parseArray(key_1.toString(),Integer.class);
        System.out.println(int_list.get(1)+1);
        System.out.println(key_duration);*/





        // 点击事件
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = page - 1;
                for (int i = 0; i < lightList.size(); i++) {
                    Button button_change = lightList.get(i);
                    if(data[page][i] == 1){

                        button_change.setBackgroundColor(0xFF00FF00);

                    }else if(data[page][i] == 2){

                        button_change.setBackgroundColor(0xFF0000FF);

                    }else if(data[page][i] == 3){

                        button_change.setBackgroundColor(0xFFFF0000);

                    }else if(data[page][i] == 4){

                        button_change.setBackgroundColor(0xFFFFFF00);

                    }else{

                        button_change.setBackgroundResource(R.drawable.button_border);

                    }
                }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = page + 1;
                for (int i = 0; i < lightList.size(); i++) {
                    Button button_change = lightList.get(i);
                    if(data[page][i] == 1){

                        button_change.setBackgroundColor(0xFF00FF00);

                    }else if(data[page][i] == 2){

                        button_change.setBackgroundColor(0xFF0000FF);

                    }else if(data[page][i] == 3){

                        button_change.setBackgroundColor(0xFFFF0000);

                    }else if(data[page][i] == 4){

                        button_change.setBackgroundColor(0xFFFFFF00);

                    }else{

                        button_change.setBackgroundResource(R.drawable.button_border);

                    }
                }
            }
        });

        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                for (int i = 0; i < lightList.size(); i++) {
                    Button button_change = lightList.get(i);
                    button_change.setBackgroundResource(R.drawable.button_border);
                }
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i =-0 ; i < 1000; i++){
//                    if( i % 2 == 0 ){
//                        g_src = g_activity.load("http://inews.gtimg.com/newsapp_bt/0/11352303298/641");
//                    }else{
//                        g_src = g_activity.load("http://inews.gtimg.com/newsapp_bt/0/11352303309/641");
//                    }
                    RequestManager g_activity = Glide.with(MainActivity.this);
                    RequestBuilder<Drawable> g_src = g_activity.load("https://www.music-for-music-teachers.com/images/xhome-on-the-range-guitar-simple-g.gif.pagespeed.ic.kRuE-Nncb9.webp");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            g_src.into(show_img); // http://inews.gtimg.com/newsapp_bt/0/11352303309/641
                        }
                    });
                }
            }
        }).start();


        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // page = 1;
                if_display = 0;
//                for (int i = 0; i < lightList.size(); i++) {
//                    Button button_change = lightList.get(i);
//                    button_change.setBackgroundResource(R.drawable.button_border);
//                }
            }
        });

        btn_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //page = 1;
                if_display = 1;
                if_sleep = 1;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(page < MaxPage){
                            if(if_display == 1){
                                page = page + 1;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < lightList.size(); i++) {
                                            Button button_change = lightList.get(i);
                                            if(data[page][i] == 1){

                                                button_change.setBackgroundColor(0xFF00FF00);

                                            }else if(data[page][i] == 2){

                                                button_change.setBackgroundColor(0xFF0000FF);

                                            }else if(data[page][i] == 3){

                                                button_change.setBackgroundColor(0xFFFF0000);

                                            }else if(data[page][i] == 4){

                                                button_change.setBackgroundColor(0xFFFFFF00);

                                            }else{

                                                button_change.setBackgroundResource(R.drawable.button_border);
                                            }
                                        }
                                    }
                                });
                            }else{
                                if_sleep = 0;
//                                page = 1;
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//
//                                    }
//                                });
                            }
                            if(if_sleep == 1){
                                try {
                                    Thread.sleep(duration_data[page]);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        page = 1;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < lightList.size(); i++) {
                                    Button button_change = lightList.get(i);
                                    button_change.setBackgroundResource(R.drawable.button_border);
                                }
                            }
                        });
                    }
                }).start();
            }
        });

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for(int i = 0; i < 1000; i++){
//                    if(i%2 == 0){
//                        Glide.with(MainActivity.this)
//                                .load("http://inews.gtimg.com/newsapp_bt/0/11352303298/641") // http://inews.gtimg.com/newsapp_bt/0/11352303298/641
//                                .into(show_img); // http://inews.gtimg.com/newsapp_bt/0/11352303309/641
//                    }else{
//                        Glide.with(MainActivity.this)
//                                .load("http://inews.gtimg.com/newsapp_bt/0/11352303309/641") // http://inews.gtimg.com/newsapp_bt/0/11352303298/641
//                                .into(show_img); // http://inews.gtimg.com/newsapp_bt/0/11352303309/641
//                    }
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();


//        btn_download_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Glide.with(MainActivity.this)
//                        .load("http://127.0.0.1:8020/static/pu1.jpg") // http://inews.gtimg.com/newsapp_bt/0/11352303298/641
//                        .into(show_img); // http://inews.gtimg.com/newsapp_bt/0/11352303309/641
//            }
//        });


    }
    public void downloadJsonFile(View view){
        new Thread(){
            @Override
            public void run(){
                FileDownloader fileDownloader = new FileDownloader();
                try {
                    fileDownloader.downloadJsonFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    //Request request = new Request.Builder().url("https://www.httpbin.org/get?a=1&b=2").build();
    //prepare a Call object
    //Call call = okHttpClient.newCall(request);

    public void getFile(View view){
        new Thread(){
            @Override
            public void run(){
                Request request = new Request.Builder().url("https://www.httpbin.org/get?a=1&b=2").build();
                //prepare a Call object
                Call call = okHttpClient.newCall(request);
                try {
                    Response response = call.execute();
                    Log.i(TAG, "getFile" + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }



    public void postAsync(View view){
        retrofit2.Call<ResponseBody> call = httpbinService.post("lance", "123");
        call.enqueue(new retrofit2.Callback<ResponseBody>(){
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response){
                try {
                    Log.i(TAG, "postAsync" + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t){
            }
        });
    }


    public void getImageFilePu(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    retrofit2.Response<ResponseBody> response = downloadFileTool.download("http://127.0.0.1:9000/download/1/").execute();
                    InputStream inputStream = response.body().byteStream();
                    //Log.i(TAG, "getImageFilePu" + response.body().string());
                    File extDir = Environment.getExternalStorageDirectory();
                    Log.i(TAG, "getImageFilePu" + extDir);
                    FileOutputStream fos = new FileOutputStream(extDir+"tu.png");
                    int len;
                    byte[] buffer = new byte[4096];
                    while ((len = inputStream.read(buffer)) != -1){
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public void getJsonFile(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    retrofit2.Response<ResponseBody> response = downloadFileTool.download("http://127.0.0.1:9000/download/2/").execute();
                    InputStream inputStream = response.body().byteStream();
                    File extDir = Environment.getExternalStorageDirectory();
                    Log.i(TAG, "getImageFilePu" + extDir);
                    FileOutputStream fos = new FileOutputStream(extDir+"tu.png");
                    int len;
                    byte[] buffer = new byte[4096];
                    while ((len = inputStream.read(buffer)) != -1){
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}

