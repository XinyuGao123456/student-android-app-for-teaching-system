package com.example.gaofirstandroidapp;

import com.example.gaofirstandroidapp.bean.DownloadFileTool;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;


public class FileDownloader {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("http://127.0.0.1:8000/").build();
    DownloadFileTool downloadFileTool = retrofit.create(DownloadFileTool.class);
    public void downloadJsonFile() throws IOException {
        Response<ResponseBody> response = downloadFileTool.download("http://127.0.0.1:8000/download/2/").execute();
        InputStream inputStream = response.body().byteStream();
        FileOutputStream fos = new FileOutputStream("C:\\Users\\Administrator\\AndroidStudioProjects\\GaoFirstAndroidApp\\app\\src\\main\\assets\\lightEventData.json");
        int len;
        byte[] buffer = new byte[4096];
        while ((len = inputStream.read(buffer)) != -1){
            fos.write(buffer, 0, len);
        }
        fos.close();
        inputStream.close();
    }
}
