package com.example.gaofirstandroidapp;


import com.example.gaofirstandroidapp.bean.DownloadFileTool;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;



public class DownloadFileTest {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("http://127.0.0.1:8000/").build();
    DownloadFileTool downloadFileTool = retrofit.create(DownloadFileTool.class);

//    @Test  //http://127.0.0.1:8000/download/1/ https://down.caimoge.cc/modules/article/txtarticle.php?id=75516
//    public void downloadImgFile() throws IOException {
//        Response<ResponseBody> response = downloadFileTool.download("http://127.0.0.1:9000/download/1/").execute();
//        InputStream inputStream = response.body().byteStream();
//        FileOutputStream fos = new FileOutputStream("C:\\Users\\Administrator\\AndroidStudioProjects\\GaoFirstAndroidApp\\app\\src\\main\\res\\drawable\\pu.jpg");
//        int len;
//        byte[] buffer = new byte[4096];
//        while ((len = inputStream.read(buffer)) != -1){
//            fos.write(buffer, 0, len);
//        }    // https://down.caimoge.cc/modules/article/txtarticle.php?id=75516
//        fos.close();
//        inputStream.close();
//    }

    @Test
    public void downloadJsonFile() throws IOException {
        Response<ResponseBody> response = downloadFileTool.download("http://127.0.0.1:8000/download1/2/").execute(); // http://127.0.0.1:9000/download/2/
        InputStream inputStream = response.body().byteStream();
        FileOutputStream fos = new FileOutputStream("C:\\Users\\Administrator\\AndroidStudioProjects\\GaoFirstAndroidApp\\app\\src\\main\\assets\\lightEventData.json");
        int len;  //C:\Users\Administrator\AndroidStudioProjects\GaoFirstAndroidApp\app\src\main\assets
        byte[] buffer = new byte[4096];
        while ((len = inputStream.read(buffer)) != -1){
            fos.write(buffer, 0, len);
        }
        fos.close();
        inputStream.close();
    }

}
